module.exports = {
  createAccount:async (req, res) => {

    let data = _.pick(req.body, ["name", "balance"]);
    data.owner = req.params.id;

    var AccountData = await Accounts.create(data).fetch().catch(err=>{
      if (err) {
        console.log("erorrrrrrrrrrrrrrrr", err);
        return res.send(500, { error: "Database error" });
      }
      })
      if(AccountData){
        console.log('AccountData',AccountData);
        
      return res.status(200).send({ message: "Account stored successfully",AccountData});
      }
  },

  getAccounts: async (req, res) => {
    console.log(req.params.id);
    let accounts = await Accounts.find({ owner: req.params.id }).populate("friends").catch(error => {
      console.log('error in fetching account',error);
      return res.send(500, { error: "Database error" });
    });
    return res.send(200, {
      message: "accounts fetched",
      accounts
    });
  },

  editAccount: async (req, res) => {
    console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);
    var updatedAccount = await Accounts.updateOne(
      { id: req.params.id },
      { name: req.body.name }
    ).catch(error => {
        console.log('error in updating',error);
        return res.send(500, { error: "Database error" });
      });
      if(updatedAccount){
        console.log("updatedAccount",updatedAccount)

        return res.status(200).send({message:'Account updated successfully',updatedAccount})
      }
      else
      {
        console.log("updatedAccount",updatedAccount)
      }
  },

  deleteAccount: async (req, res) => {
    console.log("comes into delete----ACC")
    console.log("req.params.id", req.params.id);
    var deletedAccount = await Accounts.destroyOne({id: req.params.id})
      .catch(error => {
        console.log('error in deleting',error);
        return res.send(500, { error: "Database error" });
      });
      if(deletedAccount){
        return res.status(200).send({message:'Account deleted successfully'})
      }
  }
};
