
module.exports.routes = {


  '/': { view: 'pages/homepage' },
  "post /register":"UserController.create",
  "post /login":"UserController.login",
  "get /user/:id":"UserController.getUser",
  "get /otherUsers/:id":"UserController.getOtherUsers",
  "get /accounts/:id":"AccountsController.getAccounts",
  "post /addFriend/:email":"UserController.addFriend",
  "post /account/create/:id":"AccountsController.createAccount",
  "put /account/edit/:id":"AccountsController.editAccount",
  "delete /account/delete/:id":"AccountsController.deleteAccount",
  "get /logout":"UserController.logout",
  "get /getFriends/:id":"UserController.getFriends",

}