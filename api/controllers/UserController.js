const passport = require("passport");

module.exports = {
  create: async (req, res) => {
    try {
      response = {};
      console.log("req.body", req.body);

      let data = _.pick(req.body, ["name", "email", "password"]);
      let user = await User.create(data)
        .fetch()
      if (user) {
        // create defalut account
        let account = await Accounts.create({
          name: "default",
          balance: 0,
          owner: user.id
        })
          .fetch()
        if (account) {
          console.log("account created", user);
          var mailOptions = {
            to: "abhip@zignuts.com",
            subject: "abc",
            html:"<h1>Expense Manager</h1><br><p>Your account has been successfully created to expense manager</p>"
          };
          emailService.sendEmail(mailOptions)
          return res.status(200).send({
            message: "User created",
            user,
            account
          });
        }
      }
    } catch (error) {
      if (error && error.code === "E_UNIQUE") {
        return res.status(400).send({ message: "email already exist" });
      }
      return res.status(500).send({ message: "Database error" });

    }
  },
  getUser:async (req, res) => {
    // console.log(req.params.id)
    let user = await User.findOne({ id: req.params.id }).populate('accounts').catch(err => {
          if (err) {
            return res.status(500)({
              error: "Fetching user error"
            });
          }
        });
    return res.status(200).send({message:'user fatched successfully',user})
  },
  getOtherUsers:async (req, res) => {
    console.log("OTHWER users id purpose",req.params.id)
    let users = await User.find().catch(err => {
          if (err) {
            console.log(err);
            return res.status(500).send({
              error: "Fetching user error"
            });
          }
        });
        console.log("FETCHED UseRs----",users)
        var otherUsers = users.filter(data=>data.id!=req.params.id)
    return res.status(200).send({message:'user fatched successfully',otherUsers})
  },
  login: function(req, res) {
    passport.authenticate("local", function(err, user, info) {
      if (err || !user) return res.status(400).send({ message: info.message, user });

      req.login(user, function(err) {
        if (err) res.status(400).send(err);
       
        return res.status(200).send({
          message: "User Authenticated successfully",
          user
        });
      });
    })(req, res);
  },

  logout : function(req,res){
    req.logout();
  },

  addFriend : async(req,res)=>{
    console.log('FRIEND EmaiL :',req.params.email);
let friend = await User.findOne({email:req.params.email}).catch(err => {
  if (err) {
    return res.status(500)({
      error: "Eror in fetching friend"
    });
  }
});
//    let friend =  await User.addToCollection("5e68bca84b00cd6e27090ad8", 'friends',5e68bca84b00cd6e27090ad8);

let friendAdded = await Accounts.addToCollection(
  req.body.accountId,
  "friends",
  friend.id
).catch(err=>{
  console.log("ERRORRRRRRRRRRRRRRR",err)
  if (err) {
    return res.status(500)({
      error: "Error in adding friend"
    });
  }
})

  return res.status(200).send({message:'friend added successfully'})

  },
  getFriends:async (req, res) => {
    // console.log(req.params.id)
    let user = await User.find({ id: req.params.id }).populate('friends').catch(err => {
          if (err) {
            return res.status(500)({
              error: "Error in fetching friends"
            });
          }
        });
    return res.status(200).send({message:'Friends fatched successfully',user})
  },
// let friend = await User.addToCollection(req.params.id, 'friends')
// .members(['5e68bca84b00cd6e27090ad9']);
// console.log('friend',friend);
//     if(friend){
//       console.log('friend added successfully');
      
//     }
//   }

 

};
