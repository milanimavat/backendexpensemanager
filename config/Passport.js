const passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	bcrypt = require('bcrypt-nodejs');
	passport.serializeUser(function(user, cb) {
    console.log("serializing",user);
    
		cb(null, user.id);
	});
	passport.deserializeUser(function(id, cb){
    console.log("De - serializing with this id",id);

		User.findOne({id}, function(err, user) {
      console.log("De - serializing...",user);
			cb(err, user);
		});
	});
	passport.use(new LocalStrategy({
			usernameField: 'email',
			passportField: 'password'
		}, function(email, password, cb){
      console.log("email", email);
      console.log("password", password);

		User.findOne({email: email}, function(err, user){
			if(err) { 
        console.log("find one query error", err);
        return cb(err);
      }
      if(!user) 
      {
        console.log("user not found error", user);
      return cb(null, false, {message: 'No such user found'});
      }
      console.log("user is this", user);
      bcrypt.compare(password, user.password, function(err, res){
				if(!res) return cb(null, false, { message: 'Invalid Password' });
				return cb(null, user, { message: 'Login Succesful'});
			});
		});
	}));