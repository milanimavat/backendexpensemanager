
var bcrypt = require("bcrypt-nodejs");
module.exports = {

  attributes: {
    name:{
      type:'string',
      required: true,
    },
    email:{
      type:'string',
      required: true,
      unique: true,
      isEmail:true
    },
    password:{
      type:'string',
      required: true,
    },
    accounts:{
      collection:"Accounts",
      via: 'owner'
    },
    friends:{
      model: 'Accounts',
    }
  },
 
   beforeCreate: function(user, cb){
   bcrypt.genSalt(10, function(err, salt){
     bcrypt.hash(user.password, salt, null, function(err, hash){
     if(err) return cb(err);
     user.password = hash;
     return cb();
     });
   });
  },
  
/**
 * To use this call like User.comparePassword(old_password,whole user data object)
 */
  comparePassword: function(password, user) {
    return new Promise(function(resolve, reject) {
      bcrypt.compare(password, user.password, function(err, match) {
        if (err) resolve(false);
        if (match) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  },

};

