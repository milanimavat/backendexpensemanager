module.exports = {

errorMessage: (res, field = "Anonymous", type = "Anonymous") =>
    res
      .status(400)
      .send({ message: `Please fillup ${field} and type mustbe ${type}.`, status:400 }),
  minLengthErrorMessage: (res, field = "Anonymous", length = 0) =>
    res.status(400).send({
      message: `Minimum ${length} characters required in ${field} field.`, status:400
    }),
  includeErrorMessage: (res, field = "Anonymous", dataArray = []) =>
    res.status(400).send({ message: `${field} must be in ${dataArray}.`, status:400 }),
  queryErrorMessage: async (res, req, error ) => {
    //Error log
    await sails.helpers.errorLog.with({ req, error });
    return res.status(500).send({ message: sails.__("DATABASE_ERROR"), status:500, error });
  },
  notExistMessage: (res, message) =>
    res.status(400).send({ message, status:400}),
  alreadyExistMessage: (res, message) =>
    res.status(400).send({ message,status:400  }),
  notValidMessage: (res, message) =>
    res.status(400).send({ message,status:400 }),
  customErrorMessage: (res, message = "Anonymous") =>
    res.status(400).send({ message,status:400 }),
  /**
   * Response  Messages - 200
   */
  successMessage: (res, message, data = {}) =>
    res
      .status(200)
      .send({ message, status:200, ...data })
};